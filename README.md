# Project Quartz

## How to run

To run the game, run

```
cargo run --features "vulkan"
```

on macOS, the feature may be switched to `metal` in the case of it not running.
